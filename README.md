# [Paris-Lille-3D](http://npm3d.fr/paris-lille-3d) preprocessed data

Created by [Xavier Roynard](https://www.researchgate.net/profile/Xavier_Roynard) from [NPM3D team](http://caor-mines-paristech.fr/fr/recherche/3d-modeling/) of Centre for Robotics of Mines ParisTech. 

----------------------------------------------------------------

![Exemple de Segmentation et Classification](data_example.png)

----------------------------------------------------------------

This repository contains preprocessed files of dataset [Paris-Lille-3D](http://npm3d.fr/paris-lille-3d) used for example in [ms_deepvoxscene repository](https://github.com/xroynard/ms_deepvoxscene). 




